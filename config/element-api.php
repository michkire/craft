<?php

use craft\elements\Entry;
use craft\helpers\UrlHelper;

return [
    'endpoints' => [
        'n.json' => [
            'elementType' => Entry::class,
            'criteria' => ['section' => 'news'],
            'transformer' => function (Entry $entry) {
                return [
                    'title' => $entry->title,
                    'url' => $entry->url,
                    'jsonUrl' => UrlHelper::url("n/{$entry->id}.json"),
                    'body' => $entry->body,
                    'type' => $entry->type->name,
                    'category' => $entry->category,
                ];
            },
            "elementsPerPage" => 2,
        ],
        'n/<entryId:\d+>.json' => function ($entryId) {
            return [
                'elementType' => Entry::class,
                'criteria' => ['id' => $entryId],
                'one' => true,
                'transformer' => function (Entry $entry) {
                    return [
                        'title' => $entry->title,
                        'url' => $entry->url,
                        'type' => $entry->type,
                    ];
                },
            ];
        },
    ]
];