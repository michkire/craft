<?php
/**
 * Database Configuration
 *
 * All of your system's database connection settings go in here. You can see a
 * list of the available settings in vendor/craftcms/cms/src/config/DbConfig.php.
 */

return [
    'driver' => 'mysql',
    'server' => getenv('DATA_DB_HOST'),
    'user' => getenv('DATA_DB_USER'),
    'password' => getenv('DATA_DB_PASS'),
    'database' => 'gonano',
    'tablePrefix' => '',
    'port' => '3306'
];
